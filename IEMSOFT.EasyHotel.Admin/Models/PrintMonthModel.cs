﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class PrintMonthModel
    {
        public List<MonthReportMode> Reports { get; set; }
        public string SubHotelName { get; set; }
        public decimal RoomFeeSum { get; set; }
        public decimal ConsumeFeeSum { get; set; }
        public int BillsCountSum { get; set; }
        public decimal TotalFee { get; set; }
        public string StartMonth { get; set; }
        public string EndMonth { get; set; }
    }
}