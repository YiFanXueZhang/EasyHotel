﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.DAL
{
   public interface IDBProvider
    {
         PetaPoco.Database DB { get; }
    }
}
